package com.example.pirang.retrofit;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by pirang on 6/13/17.
 */

public interface PostService {

    @GET("posts")
    Call<List<Post>> findAllPosts();


    @GET("posts/{id}")
    Call<Post> findPostById(@Path("id") int postId);

}
